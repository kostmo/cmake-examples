cmake_minimum_required(VERSION 3.10)

project(myproject C)


add_subdirectory(header-generator)


add_library(foo OBJECT impl.c generated-header.h)

target_include_directories(foo PRIVATE incdir)

# This is needed to establish the generated header dependency
target_include_directories(foo PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

add_executable(my_program hello.c $<TARGET_OBJECTS:foo>)

target_include_directories(my_program PRIVATE incdir)
